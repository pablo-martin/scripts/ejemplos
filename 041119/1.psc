Algoritmo dosNumeros
	// comparar dos numeros
	// introducidos por teclado
	numero1 <- 0
	numero2 <- 0
	Escribir 'Introduce un numero'
	Leer numero1
	Escribir 'Introduce un numero'
	Leer numero2
	
	Escribir	"el primer numero vale ",numero1, " y el segundo ", numero2
	Si numero1>numero2 Entonces
		Escribir "El mayor es el ", numero1
	SiNo
		Si numero1=numero2 Entonces
			Escribir "Son iguales"
		SiNo
			Escribir "El mayor es el ", numero2
		Fin Si
	Fin Si
FinAlgoritmo

